#include "mips.hpp"

// Authors: IAN ALEXANDER AND PUSHPAL SIDHU
// CPE 315 LAB 4
// PURPOSE: IMPLEMENT MIPS INSTRUCTION SET

Stats stats;
void execute() 
{
   Data32 instr = imem[pc];
   GenericType rg(instr);
   
   stats.instrs++;
   
   // Initialize all control signals to deasserted
   // See mips.hpp for the order of the control signals
   instr.set_control(0,0,0,0,0,0,0,0,0);
   
   switch(rg.op) 
   {
   case OP_SPECIAL:
      switch(rg.func) 
      {
      case SP_ADDU:
	 // add your control signals and statistics here
	 instr.set_control(1,0,0,1,0,0,0,2,0);
	 stats.numRegWrites++;
	 stats.numRegReads += 2;
	 stats.numRType++;
	 break;
      case SP_ADD:
         instr.set_control(1,0,0,1,0,0,0,2,0);
         stats.numRegWrites++;
	 stats.numRegReads += 2;
         stats.numRType++;
         break;
      case SP_SLT:
         instr.set_control(1,0,0,1,0,0,0,2,0);
	 stats.numRegWrites++;
	 stats.numRegReads += 2;
	 stats.numRType++;
	 break;
      case SP_JR:
	 instr.set_control(0,0,0,0,0,0,1,2,0);
	 stats.numRegReads++;
	 stats.numRType++;
	 break;
      case SP_SLL:
         instr.set_control(1,0,0,1,0,0,0,2,0);
	 stats.numRegWrites++;
	 stats.numRegReads++;
	 stats.numRType++;
	 break;
      case SP_SRL:
         instr.set_control(1,0,0,1,0,0,0,2,0);
	 stats.numRegWrites++;
	 stats.numRegReads += 2;
	 stats.numRType++;
	 break;
      case SP_SRA:
         instr.set_control(1,0,0,1,0,0,0,2,0);
	 stats.numRegWrites++;
	 stats.numRegReads++;
	 stats.numRType++;
	 break;
      case SP_MULT:
         instr.set_control(1,0,0,1,0,0,0,2,0);
	 stats.numRegWrites++;
	 stats.numRegReads += 2;
	 stats.numRType++;
	 break;
      case SP_MULTU:
         instr.set_control(1,0,0,1,0,0,0,2,0);
	 stats.numRegWrites++;
	 stats.numRegReads += 2;
	 stats.numRType++;
	 break;
      case SP_SUB:
         instr.set_control(1,0,0,1,0,0,0,2,0);
	 stats.numRegWrites++;
	 stats.numRegReads += 2;
	 stats.numRType++;
	 break;
      case SP_SUBU:
         instr.set_control(1,0,0,1,0,0,0,2,0);
	 stats.numRegWrites++;
	 stats.numRegReads += 2;
	 stats.numRType++;
	 break;
      case SP_AND:
         instr.set_control(1,0,0,1,0,0,0,2,0);
	 stats.numRegWrites++;
	 stats.numRegReads += 2;
	 stats.numRType++;
	 break;
      case SP_OR:
         instr.set_control(1,0,0,1,0,0,0,2,0);
	 stats.numRegWrites++;
	 stats.numRegReads += 2;
	 stats.numRType++;
	 break;
      case SP_XOR:
         instr.set_control(1,0,0,1,0,0,0,2,0);
	 stats.numRegWrites++;
	 stats.numRegReads += 2;
	 stats.numRType++;
	 break;
      case SP_NOR:
         instr.set_control(1,0,0,1,0,0,0,2,0);
	 stats.numRegWrites++;
	 stats.numRegReads += 2;
	 stats.numRType++;
	 break;
      case SP_JALR:
	 instr.set_control(1,0,0,1,0,0,1,2,0);
	 stats.numRegWrites++;
	 stats.numRegReads++;
	 stats.numRType++;
	 break;
      default:
	 cout << "Unsupported instruction: ";
	 instr.printI(instr);
	 exit(1);
	 break;
      }
      break;
   case OP_ADDIU:
      // add your control signals and statistics here
      instr.set_control(1,1,0,1,0,0,0,0,0);
      stats.numRegWrites++;
      stats.numRegReads++;
      stats.numIType++;
      break;
   case OP_ORI:
      instr.set_control(1,1,0,1,0,0,0,1,0);
      stats.numRegWrites++;
      stats.numRegReads++;
      stats.numIType++;
      break;
   case OP_SW:
      instr.set_control(1,1,0,0,0,1,0,0,0);
      stats.numRegReads += 2;
      stats.numMemWrites++;
      stats.numIType++;
      break;
   case OP_SB:      
      instr.set_control(1,1,0,0,0,1,0,0,0);
      stats.numRegReads += 2;
      stats.numMemWrites++;
      stats.numIType++;
      break;
   case OP_LW:
      instr.set_control(1,1,1,1,1,0,0,0,0);
      stats.numMemReads++;
      stats.numRegWrites++;
      stats.numRegReads++;
      stats.numIType++;
      break;
   case OP_LBU:
      instr.set_control(1,1,1,1,1,0,0,0,0);
      stats.numMemReads++;
      stats.numRegReads++;
      stats.numRegWrites++;
      stats.numIType++;
      break;
   case OP_LB:
      instr.set_control(1,1,1,1,1,0,0,0,0);
      stats.numMemReads++;
      stats.numRegReads++;
      stats.numRegWrites++;
      stats.numIType++;
      break;
   case OP_SLTI:
      instr.set_control(1,1,0,1,0,0,0,2,0);
      stats.numRegWrites++;
      stats.numRegReads++;
      stats.numIType++;
      break;
   case OP_SLTIU:
      instr.set_control(1,1,0,1,0,0,0,2,0);
      stats.numRegWrites++;
      stats.numRegReads++;
      stats.numIType++;
      break;
   case OP_BNE:
      instr.set_control(0,0,0,0,0,0,1,1,0);
      stats.numBranches++;
      stats.numRegReads += 2;
      stats.numIType++;
      break;
   case OP_BEQ:
      instr.set_control(0,0,0,0,0,0,1,1,0);
      stats.numBranches++;
      stats.numRegReads += 2;
      stats.numIType++;
      break;
   case OP_BLEZ:
      instr.set_control(0,0,0,0,0,0,1,1,0);
      stats.numBranches++;
      stats.numRegReads += 2;
      stats.numIType++;
      break;
   case OP_LUI:
      instr.set_control(1,1,0,1,0,0,0,0,0);
      stats.numRegWrites++;
      stats.numIType++;
      break;
   case OP_JAL:
      instr.set_control(1,0,0,1,0,0,0,0,1);
      stats.numRegWrites++;
      stats.numJType++;
      break;
   case OP_J:
      instr.set_control(0,0,0,0,0,0,0,0,1);
      stats.numJType++;
      break;
   default:
      cout << "Unsupported instruction: ";
      instr.printI(instr);
      exit(1);
      break;
   }
   instr.printI(instr);
}
