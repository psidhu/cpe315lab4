TARGETS         = mipscontrol
SRCS            = classify.cc execute.cc main.cc 
HEADERS         = mips.hpp
OBJS            = $(SRCS:.cc=.o) parse
srcdir          = .
INCLUDES	= -I$(srcdir)
#CXXFLAGS          = -g
CXXFLAGS        = -O3
LDFLAGS		= 
LIBS		= -lstdc++

default: $(TARGETS)

all: default

$(TARGETS): $(OBJS) $(HEADERS)
	$(CXX) $(LDFLAGS) -o $@ $(OBJS) $(LIBS)

clean:  
	-rm -f *.o $(TARGETS)
