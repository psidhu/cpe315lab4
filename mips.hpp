#ifndef _MIPS_H_
#define _MIPSIM_H_

#include <iostream>
#include <vector>
#include <algorithm>
#include <map>
#include <cmath>

using namespace std;

#undef LITTLE_ENDIAN
#undef BIG_ENDIAN

#ifdef __i386__
#define LITTLE_ENDIAN
#endif
#ifdef __x86_64__
#define LITTLE_ENDIAN
#endif
#ifdef _BIG_ENDIAN
#define BIG_ENDIAN
#endif

enum Type { R_TYPE, I_TYPE, J_TYPE, GENERIC_TYPE };

enum Op {
  OP_SPECIAL, OP_REGIMM, OP_J, OP_JAL, OP_BEQ, OP_BNE, OP_BLEZ, OP_BGTZ,
  OP_ADDI, OP_ADDIU, OP_SLTI, OP_SLTIU, OP_ANDI, OP_ORI, OP_XORI, OP_LUI,
  OP_COP0, OP_COP1, OP_COP2, OP_23, OP_BEQL, OP_BNEL, OP_BLEZL, OP_BGTZL,
  OP_DADDI, OP_DADDIU, OP_LDL, OP_LDR, OP_34, OP_35, OP_36, OP_37,
  OP_LB, OP_LH, OP_LWL, OP_LW, OP_LBU, OP_LHU, OP_LWR, OP_LWU,
  OP_SB, OP_SH, OP_SWL, OP_SW, OP_SBU, OP_SHU, OP_SWR, OP_SWU,
  OP_UNDEFINED
};

enum Special {
  SP_SLL, SP_01, SP_SRL, SP_SRA, SP_SLLV, SP_05, SP_SRLV, SP_SRAV,
  SP_JR, SP_JALR, SP_12, SP_13, SP_SYSCALL, SP_BREAK, SP_16, SP_SYNC,
  SP_MFHI, SP_MTHI, SP_MFLO, SP_MTLO, SP_DSLLV, SP_25, SP_DSRLV, SP_DSRAV,
  SP_MULT, SP_MULTU, SP_DIV, SP_DIVU, SP_DMULT, SP_DMULTU, SP_DDIV, SP_DDIVU,
  SP_ADD, SP_ADDU, SP_SUB, SP_SUBU, SP_AND, SP_OR, SP_XOR, SP_NOR, 
  SP_50, SP_51, SP_SLT, SP_SLTU,
  SP_UNDEFINED
};

extern vector<string> opmap;
string opToString(const Op op);
extern vector<string> spmap;
string specialToString(const Special sp);
void op_init();
//unsigned int swizzle(unsigned int d);

struct ControlType {
  unsigned int RegDst   : 1;
  unsigned int ALUSrc   : 1;
  unsigned int MemtoReg : 1;
  unsigned int RegWrite : 1;
  unsigned int MemRead  : 1;
  unsigned int MemWrite : 1;
  unsigned int Branch   : 1;
  unsigned int ALUOp    : 2;
  unsigned int Jump     : 1;
};

struct RType {
#ifdef BIG_ENDIAN
  unsigned int op : 6;
  unsigned int rs : 5;
  unsigned int rt : 5;
  unsigned int rd : 5;
  unsigned int sa : 5;
  unsigned int func : 6;
#endif /* BIG_ENDIAN */
#ifdef LITTLE_ENDIAN
  unsigned int func : 6;
  unsigned int sa : 5;
  unsigned int rd : 5;
  unsigned int rt : 5;
  unsigned int rs : 5;
  unsigned int op : 6;
#endif /* LITTLE_ENDIAN */
  ControlType ct;
};

struct IType {
#ifdef BIG_ENDIAN
  unsigned int op : 6;
  unsigned int rs : 5;
  unsigned int rt : 5;
  short imm : 16;
#endif /* BIG_ENDIAN */
#ifdef LITTLE_ENDIAN
  short imm : 16;
  unsigned int rt : 5;
  unsigned int rs : 5;
  unsigned int op : 6;
#endif /* LITTLE_ENDIAN */
  ControlType ct;
};

struct JType {
#ifdef BIG_ENDIAN
  unsigned int op : 6;
  int target : 26;
#endif /* BIG_ENDIAN */
#ifdef LITTLE_ENDIAN
  int target : 26;
  unsigned int op : 6;
#endif /* LITTLE_ENDIAN */
  ControlType ct;
};

struct GenericType {
#ifdef BIG_ENDIAN
  unsigned int op : 6;
  unsigned int chud : 20;
  unsigned int func : 6;
#endif /* BIG_ENDIAN */
#ifdef LITTLE_ENDIAN
  unsigned int func : 6;
  unsigned int chud : 20;
  unsigned int op : 6;
#endif /* LITTLE_ENDIAN */
  ControlType ct;
};

class Data8 {
  private:
    unsigned char d;
  public:
    Data8() {}
    Data8(unsigned char _d) : d(_d) {}
    operator unsigned char() const { return d; }
};


class Data32 {
private:
  union {
    unsigned int _uint;
    int _int;
    unsigned char _ubyte4[4];
    RType _rtype;
    IType _itype;
    JType _jtype;
    GenericType _gtype;
  } d;
public:
  // Data32() {}
  Data32(const unsigned int & _d) { d._uint = _d; }
  Data32(const unsigned char & d0, 
         const unsigned char & d1, 
         const unsigned char & d2, 
         const unsigned char & d3) { 
    set_data_ubyte4(0, d0);
    set_data_ubyte4(1, d1);
    set_data_ubyte4(2, d2);
    set_data_ubyte4(3, d3);
  }
  operator unsigned int() const { return data_uint(); }
  bool operator==(const Data32 & dd) const { return (data_uint() == dd.data_uint()); }
  bool operator!=(const Data32 & dd) const { return (data_uint() != dd.data_uint()); }
  inline void set_data_int(int arg) { 
#ifdef LITTLE_ENDIAN
    // d._int = swizzle(arg);
    d._int = arg; 
#endif
#ifdef BIG_ENDIAN
    d._int = arg; 
#endif
  }
  inline int data_int(void) const {
#ifdef LITTLE_ENDIAN
    // return swizzle(d._int);
    return d._int; 
#endif
#ifdef BIG_ENDIAN
    return d._int; 
#endif
  }
  inline void set_data_uint(unsigned int arg) {
#ifdef LITTLE_ENDIAN
    // d._uint = swizzle(arg);
    d._uint = arg;
#endif
#ifdef BIG_ENDIAN
    d._uint = arg;
#endif
  }
  inline unsigned int data_uint(void) const {
#ifdef LITTLE_ENDIAN
    // return swizzle(d._uint);
    return d._uint; 
#endif
#ifdef BIG_ENDIAN
    return d._uint; 
#endif
  }
  inline void set_data_ubyte4(int i, unsigned char arg) {
#ifdef LITTLE_ENDIAN
    d._ubyte4[3-i] = arg;
#endif
#ifdef BIG_ENDIAN
    d._ubyte4[i] = arg;
#endif
  }
  inline unsigned char data_ubyte4(int i) const {
#ifdef LITTLE_ENDIAN
    return d._ubyte4[3-i]; 
#endif
#ifdef BIG_ENDIAN
    return d._ubyte4[i]; 
#endif
  }
  operator RType() const { return d._rtype; }
  operator IType() const { return d._itype; }
  operator JType() const { return d._jtype; }
  operator GenericType() const { return d._gtype; }
  inline void set_control(unsigned int RD, unsigned int AS, unsigned int MtR, 
      unsigned int RW, unsigned int MR, unsigned int MW, unsigned int B, 
      unsigned int AO, unsigned int J) {
    d._gtype.ct.RegDst   = RD;
    d._gtype.ct.ALUSrc   = AS;
    d._gtype.ct.MemtoReg = MtR;
    d._gtype.ct.RegWrite = RW;
    d._gtype.ct.MemRead  = MR;
    d._gtype.ct.MemWrite = MW;
    d._gtype.ct.Branch   = B;
    d._gtype.ct.ALUOp    = AO;
    d._gtype.ct.Jump     = J;
  }
  static Type classifyType(const Data32 d) {
    GenericType rg(d);
    switch(rg.op) {
    case OP_J:
    case OP_JAL:
      return J_TYPE;
      break;
    case OP_SPECIAL:
      return R_TYPE;
      break;
    default:
      return I_TYPE;
      break;
    }
  }
  static void printI(const Data32 d) {
    RType rt(d); IType it(d); JType jt(d);
    GenericType gt(d);
    switch (classifyType(d)) {
    case R_TYPE:
      cout << hex
           << "op: " << opmap[rt.op]
           << " rs: " << rt.rs
           << " rt: " << rt.rt
           << " rd: " << rt.rd
           << " sa: " << rt.sa
           << " func: " << spmap[rt.func];
      break;
    case I_TYPE:
      cout << hex
           << "op: " << opmap[it.op]
           << " rs: " << it.rs
           << " rt: " << it.rt
           << " imm: " << it.imm;
      break;
    case J_TYPE:
      cout << hex
           << "op: " << opmap[jt.op]
           << " target: " << jt.target;
      break;
    }
    cout << hex << endl 
                << "            RegDst: " << gt.ct.RegDst 
                << " ALUSrc: " << gt.ct.ALUSrc 
                << " MemtoReg: " << gt.ct.MemtoReg 
                << " RegWrite: " << gt.ct.RegWrite 
                << " MemRead: " << gt.ct.MemRead 
                << " MemWrite: " << gt.ct.MemWrite 
                << " Branch: " << gt.ct.Branch 
                << " ALUOp: " << gt.ct.ALUOp
                << " Jump: " << gt.ct.Jump << endl;
  }
  static void printD(const Data32 d) {
    cout << hex << d.data_uint() << endl;
  }
};

enum MemType { MEM_MEM, MEM_RF, MEM_INVALID };

enum DataType { INSTRUCTIONS, DATA };

template<class Stored, class Accessed>
class Memory {
private:
  vector<Stored> m;
  unsigned int base;
  unsigned int lowest, highest;
public:
  Memory() : m(0), base(0), lowest(0xffffffff), highest(0) {}
  Memory(unsigned int _base) : m(0), base(_base), lowest(0xffffffff),
                               highest(0) {}
  Memory(unsigned int size, Stored d) : m(size, d), base(0), 
                                        lowest(0xffffffff), highest(0) {}
  unsigned int size() const { return m.size(); }
  typename vector<Stored>::const_iterator begin() const { return m.begin(); }
  typename vector<Stored>::const_iterator end() const { return m.end(); }
  void write(const unsigned int addr, const Accessed data);
  const Accessed operator[](const unsigned int addr) const;
  // Accessed & operator[](const unsigned int addr);
  void dump(DataType dt) const;
  unsigned int getBase() const { return base; }
  bool inRange(unsigned int r) { return (r >= base) && (r < base + size()); } 
};

class Register {
private:
  Data32 d;
public:
  Register() : d(0) {}
  Register(Data32 _d) : d(_d) {}
  void write(unsigned int val) {
    d = Data32(val);
  }
  operator Data32() const { return d; }
  operator unsigned int() const { return d.data_uint(); }
  Register & operator=(unsigned int val) {
    write(val);
  }
};

class Stats {
public:
  unsigned int instrs;

  unsigned int numMemWrites;
  unsigned int numMemReads;
  unsigned int numRType;
  unsigned int numIType;
  unsigned int numJType;
  unsigned int numBranches;
  unsigned int numRegWrites;
  unsigned int numRegReads;

  void print();
};

class Options {
public:
  Options() : program(false), dump(false), instrs(false), writes(false),
              stats(false) {}
  bool program;
  bool dump;
  bool instrs;
  bool writes;
  bool stats;
};

extern Memory<Data8,Data32> imem;
extern Memory<Data8,Data32> dmem;
extern Register pc;
extern Stats stats;
extern Options opts;

void parse(const char * file);
void execute();  
#endif /* _MIPS_H_ */
