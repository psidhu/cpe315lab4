#include <iomanip>
#include "mips.hpp"
#include "unistd.h"

Memory<Data8,Data32> imem(0x400000);
Memory<Data8,Data32> dmem(0xffff0000);
Register pc;
Options opts;

int main(int argc, char ** argv) {
  int c;
  string filename;
  while ((c = getopt(argc, argv, "pdiwsc:f:")) != -1) {
    switch(c) {
    case 's':
      opts.stats = true;
      break;
    case 'f':
      filename = optarg;
      break;
    }
  }

  if (filename == "") {
    cerr << "ERROR: no source file specified; "
         << "run this program with -f filename" << endl;
    exit(1);
  }

  op_init();

  parse(filename.c_str());

  cout << "Instructions:" << endl;

  cout << "Starting at PC " << hex << pc << endl;

  while(imem.inRange(pc)) {
    cout << hex << setw(10) << imem[pc] << ": ";
    execute();
    pc = pc+4;
  }

  if (opts.stats) {
    stats.print();
  }

  return 0;
}
